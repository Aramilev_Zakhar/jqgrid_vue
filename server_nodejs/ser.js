const express = require('express')
const fs = require('fs')

const app = express()
const port = 3000

let fileContent = fs.readFileSync("../index.html", "utf8");




app.get('/', (request, response) => {
    response.send(fileContent)
})



app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})